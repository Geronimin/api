package models

type Battle struct {
	Id      int            `json:"id"`
	Time    int            `json:"time"`
	Winners []BattlePlayer `json:"winners"`
	Losers  []BattlePlayer `json:"losers"`
}

type BattlePlayer struct {
	Name    string  `json:"name"`
	Loadout Loadout `json:"loadout"`
}

type BattleResponse struct {
	Battles   []Battle `json:"battles"`
	Skip      int      `json:"skip"`
	Take      int      `json:"take"`
	SyncDelay int      `json:"sync_delay"`
}
