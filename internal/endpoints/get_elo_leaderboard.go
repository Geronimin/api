package endpoints

import (
	"database/sql"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
	"albion-kills-api/internal/syncstat"
)

type getEloLeaderboardEndpoint struct {
	db *sqlx.DB
}

type GetLeaderboardParams struct {
	Type   string `query:"type" default:"slayer" description:"CD Type (slayer, stalker)"`
	Season string `query:"season" default:"" description:"Select which ladder season"`
	Search string `query:"q" default:"" description:"Search for player"`
	Weapon string `query:"weapon" default:"" description:"Filter players by weapon"`
	Skip   int    `query:"skip" default:"0" description:"Offset for paging data"`
	Take   int    `query:"take" default:"20" description:"Number of records to fetch" validate:"omitempty,max=100"`
}

func (ep *getEloLeaderboardEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/leaderboards/1v1", []fizz.OperationOption{
		fizz.Summary("Get 1v1 Elo Leaderboard,"),
		fizz.ID("get_elo_leaderboard"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getEloLeaderboardEndpoint) handle(c *gin.Context, params *GetLeaderboardParams) (*models.LeaderboardResponse, error) {
	users, err := ep.fetchPlayers(
		params.Type == "slayer",
		params.Season,
		params.Search,
		params.Skip,
		params.Take,
		params.Weapon,
	)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching leaderboard players")
	}

	response := &models.LeaderboardResponse{
		Data:      users,
		Skip:      params.Skip,
		Take:      params.Take,
		SyncDelay: int(syncstat.Status.GetDelay(ep.db).Seconds()),
	}

	return response, nil
}

func (ep *getEloLeaderboardEndpoint) fetchPlayers(
	isSlayer bool,
	season string,
	q string,
	skip,
	take int,
	weapon string,
) ([]*models.LeaderboardUser, error) {
	qPred := "(1=1)"
	if q != "" {
		qPred = "(e.name like :q)"
	}
	searchTerm := fmt.Sprintf("%%%s%%", q)

	weapPred := "(1=1)"
	if weapon != "" {
		weapPred = "(fav.weapon = :weapon)"
	}

	var tableName string
	seasonPred := "(1=1)"
	projection := "e.name, e.rank, e.rating, UNIX_TIMESTAMP(e.last_update), tw.twitch_name, fav.weapon"
	if season != "" {
		if isSlayer {
			tableName = "elo_slayer_1v1_history"
		} else {
			tableName = "elo_stalker_1v1_history"
		}

		seasonPred = "(e.season = :season)"
		projection = "e.name, e.rank, e.rating, NULL as last_update, tw.twitch_name, e.weapon"
	} else {
		if isSlayer {
			tableName = "elo_slayer_1v1"
		} else {
			tableName = "elo_stalker_1v1"
		}
	}

	query := fmt.Sprintf(`
select %s
from %s as e
left join twitch_users as tw on tw.name = e.name
left join player_fav_weapon_1v1 as fav on fav.name = e.name
where e.rank != 0
  and %s
  and %s
  and %s
order by e.rank limit :limit offset :offset`, projection, tableName, seasonPred, qPred, weapPred)

	rows, err := ep.db.NamedQuery(query, map[string]interface{}{
		"limit":  take,
		"offset": skip,
		"q":      searchTerm,
		"weapon": weapon,
		"season": season,
	})

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*models.LeaderboardUser, 0)

	for rows.Next() {
		user := &models.LeaderboardUser{}
		var twitchName sql.NullString
		var favWeapon sql.NullString
		var lastUpdate sql.NullInt64

		err = rows.Scan(&user.Name, &user.Rank, &user.Rating, &lastUpdate, &twitchName, &favWeapon)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		if twitchName.Valid {
			user.TwitchUsername = twitchName.String
		}

		if favWeapon.Valid {
			user.FavoriteWeaponItem = favWeapon.String
		}

		if lastUpdate.Valid {
			user.LastUpdate = lastUpdate.Int64
		}

		users = append(users, user)
	}

	return users, nil
}
