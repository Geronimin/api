package endpoints

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
	"time"

	"albion-kills-api/internal/models"
)

type getSeasonsEndpoint struct {
	db           *sqlx.DB
	cache        []models.Season
	cacheExpires time.Time
}

func (ep *getSeasonsEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB

	ctx.Fizz.GET("/api/seasons", []fizz.OperationOption{
		fizz.Summary("Get leaderboard seasons"),
		fizz.ID("get_seasons"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getSeasonsEndpoint) handle(c *gin.Context) ([]models.Season, error) {
	seasons, err := ep.fetchSeasons()

	if err != nil {
		return nil, errors.Trace(err)
	}

	return seasons, nil
}

func (ep *getSeasonsEndpoint) fetchSeasons() ([]models.Season, error) {
	if ep.cache != nil && time.Now().Before(ep.cacheExpires) {
		return ep.cache, nil
	}

	query := `select
    season,
    name,
    UNIX_TIMESTAMP(start_date),
    UNIX_TIMESTAMP(end_date),
    if(now() between start_date AND end_date, 1, 0) as current
from season_1v1
where start_date <= now()
order by start_date desc;`

	rows, err := ep.db.Query(query)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := make([]models.Season, 0)

	for rows.Next() {
		var season models.Season

		err = rows.Scan(
			&season.Id,
			&season.Name,
			&season.StartDate,
			&season.EndDate,
			&season.IsCurrent,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		result = append(result, season)
	}

	ep.cacheExpires = time.Now().Add(time.Minute * 30)
	ep.cache = result

	return result, nil
}
