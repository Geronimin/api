package endpoints

import (
	"database/sql"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/juju/errors"
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"

	"albion-kills-api/internal/models"
	"albion-kills-api/internal/syncstat"
)

const is1v1Expr = `(e.participant_count = 1 AND e.party_size = 1)`
const is2v2Expr = `(e.participant_count = 2 AND e.party_size = 2)`
const is5v5Expr = `(e.participant_count > 1 AND e.party_size = 5)`
const isZvZExpr = `(e.participant_count >= 4 AND e.party_size > 10)`
const isFairExpr = `(abs(cast(k.item_power as signed) - cast(v.item_power as signed)) < 80)`
const isUnfairExpr = `(abs(cast(k.item_power as signed) - cast(v.item_power as signed)) > 150)`

type getPlayerEventsEndpoint struct {
	db          *sqlx.DB
	itemNameMap map[string]string
}

type GetPlayerEventsParams struct {
	Name        string `path:"name"`
	Search      string `query:"q" default:"" description:"Search for player"`
	OwnWeapon   string `query:"own_weapon" default:"" description:"Filter events by this players weapon"`
	OtherWeapon string `query:"other_weapon" default:"" description:"Filter events by opponent weapon"`
	Skip        int    `query:"skip" default:"0" description:"Offset for paging data" validate:"omitempty,max=5000"`
	Take        int    `query:"take" default:"20" description:"Number of records to fetch" validate:"omitempty,max=50"`
	BattleSize  string `query:"battle_size" default:"" description:"Filter events based on battle size (1v1, 2v2, 5v5, zvz)"`
	Sort        string `query:"sort" default:"time" description:"Sort events by time or fame"`
	ShowKills   bool   `query:"show_kills" default:"true" description:"Include kill events"`
	ShowDeaths  bool   `query:"show_deaths" default:"true" description:"Include death events"`
	ShowAssists bool   `query:"show_assists" default:"true" description:"Include assist events"`
}

func (ep *getPlayerEventsEndpoint) setup(ctx *EndpointCtx) {
	ep.db = ctx.DB
	ep.itemNameMap = ctx.ItemNameMap

	ctx.Fizz.GET("/api/players/:name/events", []fizz.OperationOption{
		fizz.Summary("Get events for player"),
		fizz.ID("get_player_events"),
	}, tonic.Handler(ep.handle, 200))
}

func (ep *getPlayerEventsEndpoint) handle(c *gin.Context, params *GetPlayerEventsParams) (*models.EventsResponse, error) {
	events, err := ep.fetchPlayerEvents(
		params.Name,
		params.Take,
		params.Skip,
		params.Search,
		params.Sort,
		params.BattleSize,
		params.OwnWeapon,
		params.OtherWeapon,
		params.ShowAssists,
		params.ShowKills,
		params.ShowDeaths,
	)

	if err != nil {
		return nil, errors.Annotate(err, "Failed fetching player events")
	}

	r := &models.EventsResponse{
		Events:    events,
		Skip:      params.Skip,
		Take:      params.Take,
		SyncDelay: int(syncstat.Status.GetDelay(ep.db).Seconds()),
	}

	return r, nil
}

func (ep *getPlayerEventsEndpoint) getProjection() string {
	return fmt.Sprintf(
		`e.event_id, e.battle_id, UNIX_TIMESTAMP(e.time) as time, e.total_kill_fame, e.participant_count, e.party_size, k.name as k_name,
k.is_primary, k.kill_fame, k.damage_done, k.healing_done, k.item_power as k_item_power,
k.guild_name as k_guild_name, k.alliance_name as k_alliance_name, kvod.link as k_vod, v.name as v_name, v.item_power as v_item_power, v.guild_name as v_guild_name,
v.alliance_name as v_alliance_name, vvod.link as v_vod, kl.main_hand_item as k_main_hand_item, kl.main_hand_tier as k_main_hand_tier, kl.main_hand_enchant as k_main_hand_enchant, kl.main_hand_quality as k_main_hand_quality,
kl.off_hand_item as k_off_hand_item, kl.off_hand_tier as k_off_hand_tier, kl.off_hand_enchant as k_off_hand_enchant, kl.off_hand_quality as k_off_hand_quality, kl.head_item as k_head_item,
kl.head_tier as k_head_tier, kl.head_enchant as k_head_enchant, kl.head_quality as k_head_quality, kl.body_item as k_body_item, kl.body_tier as k_body_tier,
kl.body_enchant as k_body_enchant, kl.body_quality as k_body_quality, kl.shoe_item as k_shoe_item, kl.shoe_tier as k_shoe_tier, kl.shoe_enchant as k_shoe_enchant,
kl.shoe_quality as k_shoe_quality, kl.bag_item as k_bag_item, kl.bag_tier as k_bag_tier, kl.bag_enchant as k_bag_enchant, kl.bag_quality as k_bag_quality,
kl.cape_item as k_cape_item, kl.cape_tier as k_cape_tier, kl.cape_enchant as k_cape_enchant, kl.cape_quality as k_cape_quality, kl.mount_item as k_mount_item,
kl.mount_tier as k_mount_tier, kl.mount_quality as k_mount_quality, kl.food_item as k_food_item, kl.food_tier as k_food_tier, kl.food_enchant as k_food_enchant,
kl.potion_item as k_potion_item, kl.potion_tier as k_potion_tier, kl.potion_enchant as k_potion_enchant, vl.main_hand_item as v_main_hand_item, vl.main_hand_tier as v_main_hand_tier,
vl.main_hand_enchant as v_main_hand_enchant, vl.main_hand_quality as v_main_hand_quality, vl.off_hand_item as v_off_hand_item, vl.off_hand_tier as v_off_hand_tier, vl.off_hand_enchant as v_off_hand_enchant,
vl.off_hand_quality as v_off_hand_quality, vl.head_item as v_head_item, vl.head_tier as v_head_tier, vl.head_enchant as v_head_enchant, vl.head_quality as v_head_quality,
vl.body_item as v_body_item, vl.body_tier as v_body_tier, vl.body_enchant as v_body_enchant, vl.body_quality as v_body_quality, vl.shoe_item as v_shoe_item,
vl.shoe_tier as v_shoe_tier, vl.shoe_enchant as v_shoe_enchant, vl.shoe_quality as v_shoe_quality, vl.bag_item as v_bag_item, vl.bag_tier as v_bag_tier,
vl.bag_enchant as v_bag_enchant, vl.bag_quality as v_bag_quality, vl.cape_item as v_cape_item, vl.cape_tier as v_cape_tier, vl.cape_enchant as v_cape_enchant,
vl.cape_quality as v_cape_quality, vl.mount_item as v_mount_item, vl.mount_tier as v_mount_tier, vl.mount_quality as v_mount_quality, vl.food_item as v_food_item,
vl.food_tier as v_food_tier, vl.food_enchant as v_food_enchant, vl.potion_item as v_potion_item, vl.potion_tier as v_potion_tier, vl.potion_enchant as v_potion_enchant,
%s as '1v1',
%s as '2v2',
%s as '5v5',
%s as 'zvz',
%s as 'fair',
%s as 'unfair', elo.points_awarded, elo.points_lost`,
		is1v1Expr,
		is2v2Expr,
		is5v5Expr,
		isZvZExpr,
		isFairExpr,
		isUnfairExpr,
	)
}

func (ep *getPlayerEventsEndpoint) usingFilters(
	q,
	sort,
	battleSize,
	ownWeapon,
	otherWeapon string,
	showAssists bool,
	showKills bool,
	showDeaths bool,
) bool {
	return (q != "" ||
		battleSize != "" ||
		ownWeapon != "" ||
		otherWeapon != "" ||
		showKills == false ||
		showAssists == false ||
		showDeaths == false ||
		sort != "time")
}

func (ep *getPlayerEventsEndpoint) fetchPlayerEvents(
	name string,
	take,
	skip int,
	q,
	sort,
	battleSize,
	ownWeapon,
	otherWeapon string,
	showAssists bool,
	showKills bool,
	showDeaths bool,
) ([]models.Event, error) {
	projection := ep.getProjection()

	searchPred := "(1=1)"
	if q != "" {
		searchPred = `
        (
        v.name LIKE :q OR
        v.guild_name LIKE :q OR
        v.alliance_name LIKE :q OR
        k.name LIKE :q OR
        k.guild_name LIKE :q OR
        k.alliance_name LIKE :q
        )`
	}
	searchTerm := fmt.Sprintf("%%%s%%", q)

	battleSizePred := battleSizeToQuery(battleSize)

	ownWeaponPredKiller := "(1=1)"
	ownWeaponPredVictim := "(1=1)"
	if ownWeapon != "" {
		ownWeaponPredKiller = `(kl.main_hand_item = :ownWeapon)`
		ownWeaponPredVictim = `(vl.main_hand_item = :ownWeapon)`
	}

	otherWeaponPredKiller := "(1=1)"
	otherWeaponPredVictim := "(1=1)"
	if otherWeapon != "" {
		otherWeaponPredKiller = `(vl.main_hand_item = :otherWeapon)`
		otherWeaponPredVictim = `(kl.main_hand_item = :otherWeapon)`
	}

	primaryPredKiller := "(1=0)"
	if showKills && showAssists {
		primaryPredKiller = "(1=1)"
	} else if showKills {
		primaryPredKiller = "(k.is_primary = 1)"
	} else if showAssists {
		primaryPredKiller = "(k.is_primary = 0)"
	}

	primaryPredVictim := "(1=0)"
	if showDeaths {
		primaryPredVictim = "(k.is_primary = 1)"
	}

	var mainQuery string
	if ep.usingFilters(q, sort, battleSize, ownWeapon, otherWeapon, showAssists, showKills, showDeaths) {
		eventsQuery := fmt.Sprintf(`
    select
        %s
    from events as e
    join killers as k on e.event_id = k.event_id
    join victims as v on e.event_id  = v.event_id
    join loadouts as kl on k.loadout = kl.id
    join loadouts as vl on v.loadout = vl.id
    left join twitch_vods as kvod on kvod.event_id = e.event_id and k.name = kvod.name
    left join twitch_vods as vvod on vvod.event_id = e.event_id and v.name = vvod.name
    left join elo_transaction_1v1 as elo on elo.event_id = e.event_id and k.name = elo.winner_name
    where k.name = :name
      and %s
      and %s
      and %s
      and %s
      and %s
    union all
     select
        %s
    from events as e
    join killers as k on e.event_id = k.event_id
    join victims as v on e.event_id  = v.event_id
    join loadouts as kl on k.loadout = kl.id
    join loadouts as vl on v.loadout = vl.id
    left join twitch_vods as kvod on kvod.event_id = e.event_id and k.name = kvod.name
    left join twitch_vods as vvod on vvod.event_id = e.event_id and v.name = vvod.name
    left join elo_transaction_1v1 as elo on elo.event_id = e.event_id and v.name = elo.loser_name
    where v.name = :name
      and %s
      and %s
      and %s
      and %s
      and %s
        `,
			projection, primaryPredKiller, searchPred, battleSizePred, ownWeaponPredKiller, otherWeaponPredKiller,
			projection, primaryPredVictim, searchPred, battleSizePred, ownWeaponPredVictim, otherWeaponPredVictim,
		)
		orderByField := ep.sortToQueryVal(sort)

		limitClause := `LIMIT :limit OFFSET :offset`

		mainQuery = fmt.Sprintf(
			`(%s) order by %s desc %s`,
			eventsQuery,
			orderByField,
			limitClause,
		)
	} else {
		mainQuery = fmt.Sprintf(`
with
  event_ids as (
	select event_id from killers
	where name = :name
	union all
	select event_id from victims where name = :name
    order by event_id desc limit :limit offset :offset
  )
select
    %s
from events as e
join event_ids as ids on ids.event_id = e.event_id
join killers as k on e.event_id = k.event_id
join victims as v on e.event_id = v.event_id
join loadouts as kl on k.loadout = kl.id
join loadouts as vl on v.loadout = vl.id
left join twitch_vods as kvod on kvod.event_id = e.event_id and k.name = kvod.name
left join twitch_vods as vvod on vvod.event_id = e.event_id and v.name = vvod.name
left join elo_transaction_1v1 as elo on elo.event_id = e.event_id
  where (k.name = :name OR (v.name = :name and k.is_primary = 1))
  order by e.time desc limit :limit;`,
			projection,
		)
	}

	rows, err := ep.db.NamedQuery(mainQuery, map[string]interface{}{
		"name":        name,
		"limit":       take,
		"offset":      skip,
		"q":           searchTerm,
		"ownWeapon":   ownWeapon,
		"otherWeapon": otherWeapon,
	})

	if err != nil {
		return nil, fmt.Errorf("Failed to execute main query: %v\n", err)
	}

	defer rows.Close()

	events := make([]models.Event, 0)

	for rows.Next() {
		event := models.Event{}
		var pointsAwarded sql.NullInt64
		var pointsLost sql.NullInt64
		var battleId sql.NullInt64

		err = rows.Scan(
			&event.Id, &battleId, &event.Time, &event.TotalKillFame, &event.ParticipantCount, &event.PartySize, &event.Killer.Name,
			&event.Killer.IsPrimary, &event.Killer.KillFame, &event.Killer.DamageDone, &event.Killer.HealingDone, &event.Killer.ItemPower,
			&event.Killer.GuildName, &event.Killer.AllianceName, &event.Killer.DbVod, &event.Victim.Name, &event.Victim.ItemPower, &event.Victim.GuildName,
			&event.Victim.AllianceName, &event.Victim.DbVod, &event.Killer.Loadout.MainHand.Item, &event.Killer.Loadout.MainHand.Tier, &event.Killer.Loadout.MainHand.Enchant, &event.Killer.Loadout.MainHand.Quality,
			&event.Killer.Loadout.OffHand.Item, &event.Killer.Loadout.OffHand.Tier, &event.Killer.Loadout.OffHand.Enchant, &event.Killer.Loadout.OffHand.Quality, &event.Killer.Loadout.Head.Item,
			&event.Killer.Loadout.Head.Tier, &event.Killer.Loadout.Head.Enchant, &event.Killer.Loadout.Head.Quality, &event.Killer.Loadout.Body.Item, &event.Killer.Loadout.Body.Tier,
			&event.Killer.Loadout.Body.Enchant, &event.Killer.Loadout.Body.Quality, &event.Killer.Loadout.Shoe.Item, &event.Killer.Loadout.Shoe.Tier, &event.Killer.Loadout.Shoe.Enchant,
			&event.Killer.Loadout.Shoe.Quality, &event.Killer.Loadout.Bag.Item, &event.Killer.Loadout.Bag.Tier, &event.Killer.Loadout.Bag.Enchant, &event.Killer.Loadout.Bag.Quality,
			&event.Killer.Loadout.Cape.Item, &event.Killer.Loadout.Cape.Tier, &event.Killer.Loadout.Cape.Enchant, &event.Killer.Loadout.Cape.Quality, &event.Killer.Loadout.Mount.Item,
			&event.Killer.Loadout.Mount.Tier, &event.Killer.Loadout.Mount.Quality, &event.Killer.Loadout.Food.Item, &event.Killer.Loadout.Food.Tier, &event.Killer.Loadout.Food.Enchant,
			&event.Killer.Loadout.Potion.Item, &event.Killer.Loadout.Potion.Tier, &event.Killer.Loadout.Potion.Enchant, &event.Victim.Loadout.MainHand.Item, &event.Victim.Loadout.MainHand.Tier,
			&event.Victim.Loadout.MainHand.Enchant, &event.Victim.Loadout.MainHand.Quality, &event.Victim.Loadout.OffHand.Item, &event.Victim.Loadout.OffHand.Tier, &event.Victim.Loadout.OffHand.Enchant,
			&event.Victim.Loadout.OffHand.Quality, &event.Victim.Loadout.Head.Item, &event.Victim.Loadout.Head.Tier, &event.Victim.Loadout.Head.Enchant, &event.Victim.Loadout.Head.Quality,
			&event.Victim.Loadout.Body.Item, &event.Victim.Loadout.Body.Tier, &event.Victim.Loadout.Body.Enchant, &event.Victim.Loadout.Body.Quality, &event.Victim.Loadout.Shoe.Item,
			&event.Victim.Loadout.Shoe.Tier, &event.Victim.Loadout.Shoe.Enchant, &event.Victim.Loadout.Shoe.Quality, &event.Victim.Loadout.Bag.Item, &event.Victim.Loadout.Bag.Tier,
			&event.Victim.Loadout.Bag.Enchant, &event.Victim.Loadout.Bag.Quality, &event.Victim.Loadout.Cape.Item, &event.Victim.Loadout.Cape.Tier, &event.Victim.Loadout.Cape.Enchant,
			&event.Victim.Loadout.Cape.Quality, &event.Victim.Loadout.Mount.Item, &event.Victim.Loadout.Mount.Tier, &event.Victim.Loadout.Mount.Quality, &event.Victim.Loadout.Food.Item,
			&event.Victim.Loadout.Food.Tier, &event.Victim.Loadout.Food.Enchant, &event.Victim.Loadout.Potion.Item, &event.Victim.Loadout.Potion.Tier, &event.Victim.Loadout.Potion.Enchant,
			&event.Tags.Is1v1, &event.Tags.Is2v2, &event.Tags.Is5v5, &event.Tags.IsZvZ, &event.Tags.Fair, &event.Tags.Unfair, &pointsAwarded, &pointsLost,
		)

		if err != nil {
			return nil, fmt.Errorf("Failed while reading row: %v\n", err)
		}

		event.Killer.Vod = event.Killer.DbVod.String
		event.Victim.Vod = event.Victim.DbVod.String
		event.BattleId = int(battleId.Int64)

		if pointsAwarded.Valid {
			event.Killer.EloChange = &pointsAwarded.Int64
		}
		if pointsLost.Valid {
			points := -pointsAwarded.Int64
			event.Victim.EloChange = &points
		}

		event.Killer.Loadout.ReconstructIds()
		event.Victim.Loadout.ReconstructIds()

		decorateLoadoutWithItemNames(ep.itemNameMap, &event.Killer.Loadout)
		decorateLoadoutWithItemNames(ep.itemNameMap, &event.Victim.Loadout)

		events = append(events, event)
	}

	return events, nil
}

func (ep *getPlayerEventsEndpoint) sortToQueryVal(sort string) string {
	switch sort {
	case "time":
		return "time"
	case "fame":
		return "total_kill_fame"
	case "ip":
		return "item_power"
	default:
		return "time"
	}
}
